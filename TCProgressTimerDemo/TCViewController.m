//
//  TCViewController.m
//  TCProgressTimerDemo
//
//  Created by Tony Chamblee on 11/17/13.
//  Copyright (c) 2013 Tony Chamblee. All rights reserved.
//

#import "TCViewController.h"
#import "TCMyScene.h"

@implementation TCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    SKView *skView = [[SKView alloc] initWithFrame:self.view.bounds];
    [self.view insertSubview:skView atIndex:0];
        
    SKScene *scene = [TCMyScene sceneWithSize:skView.bounds.size];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    
    [skView presentScene:scene];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
    else
    {
        return UIInterfaceOrientationMaskAll;
    }
}

- (IBAction)didTapFacebookButton:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/toyrookgames"]];
}

- (IBAction)didTapTwitterButton:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/toyrookgames"]];
}

@end
